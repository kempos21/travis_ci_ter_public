package um.fds.agl.ter22.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Subject {

    private @Id @GeneratedValue Long id;
    private String title;
    private @ManyToOne Teacher teacher;
    private @ManyToOne Teacher encadrant;

    public Subject(){}
    public Subject(String title, Teacher teacher, Teacher encadrant){
        this.title = title;
        this.teacher = teacher;
        this.encadrant = encadrant;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public Teacher getEncadrant(){return encadrant;}

    public void setId(Long id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public void setEncadrant(Teacher encadrant){this.encadrant = encadrant;}
}
