package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.StudentGroup;
import um.fds.agl.ter22.forms.StudentGroupForm;
import um.fds.agl.ter22.services.StudentGroupService;
import um.fds.agl.ter22.services.TeacherService;

@Controller
public class StudentGroupController implements ErrorController {

    @Autowired
    private TeacherService teacherService;
    @Autowired
    private StudentGroupService studentGroupService;

    @GetMapping("/listStudentGroups")
    public Iterable<StudentGroup> getStudentGroups(Model model) {
        model.addAttribute("studentGroups", studentGroupService.getStudentGroups());

        return studentGroupService.getStudentGroups();
    }
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    @GetMapping(value = { "/addStudentGroup" })
    public String showAddStudentGroupPage(Model model) {

        StudentGroupForm studentGroupForm = new StudentGroupForm();
        model.addAttribute("studentGroupForm", studentGroupForm);

        return "addStudentGroup";
    }

    @PostMapping(value = { "/addStudentGroup"})
    public String addStudentGroup(Model model, @ModelAttribute("StudentGroupForm") StudentGroupForm studentGroupForm) {
        StudentGroup studentGroup;
        if(studentGroupService.findById(studentGroupForm.getId()).isPresent()){
            studentGroup = studentGroupService.findById(studentGroupForm.getId()).get();
            studentGroup.setName(studentGroupForm.getName());
        } else {
            studentGroup = new StudentGroup(studentGroupForm.getName());
        }
        studentGroupService.saveStudentGroup(studentGroup);
        return "redirect:/listStudentGroups";

    }

    @GetMapping(value = {"/showStudentGroupUpdateForm/{id}"})
    public String showStudentGroupUpdateForm(Model model, @PathVariable(value = "id") long id){

        StudentGroupForm studentGroupForm = new StudentGroupForm(id, studentGroupService.findById(id).get().getName());
        model.addAttribute("studentGroupForm", studentGroupForm);
        return "updateStudentGroup";
    }

    @GetMapping(value = {"/deleteStudentGroup/{id}"})
    public String deleteStudentGroup(Model model, @PathVariable(value = "id") long id){
        studentGroupService.deleteStudentGroup(id);
        return "redirect:/listStudentGroups";
    }
}
