package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.StudentGroup;
import um.fds.agl.ter22.repositories.StudentGroupRepository;

import java.util.Optional;


@Service
public class StudentGroupService {

    @Autowired
    private StudentGroupRepository studentGroupRepository;

    public Optional<StudentGroup> getStudentGroup(final Long id) {
        return studentGroupRepository.findById(id);
    }

    public Iterable<StudentGroup> getStudentGroups() {
        return studentGroupRepository.findAll();
    }

    public void deleteStudentGroup(final Long id) {
        studentGroupRepository.deleteById(id);
    }

    public StudentGroup saveStudentGroup(StudentGroup studentGroup) {
        StudentGroup savedStudentGroup = studentGroupRepository.save(studentGroup);
        return savedStudentGroup;
    }

    public Optional<StudentGroup> findById(long id) {
        return studentGroupRepository.findById(id);
    }
}