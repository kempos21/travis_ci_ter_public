package um.fds.agl.ter22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import um.fds.agl.ter22.entities.Subject;
import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.services.SubjectService;
import um.fds.agl.ter22.services.TeacherService;

import java.util.ArrayList;
import java.util.Iterator;

@Controller
public class SubjectController implements ErrorController {

    @Autowired
    private TeacherService teacherService;
    @Autowired
    private SubjectService subjectService;

    @GetMapping("/listSubjects")
    public Iterable<Subject> getSubjects(Model model) {
        model.addAttribute("subjects", subjectService.getSubjects());

        return subjectService.getSubjects();
    }
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER')")
    @GetMapping(value = { "/addSubject" })
    public String showAddSubjectPage(Model model) {

        SubjectForm subjectForm = new SubjectForm();
        model.addAttribute("subjectForm", subjectForm);

        return "addSubject";
    }

    @PostMapping(value = { "/addSubject"})
    public String addSubject(Model model, @ModelAttribute("SubjectForm") SubjectForm subjectForm) {
        Subject subject;
        if(subjectService.findById(subjectForm.getId()).isPresent()){
            subject = subjectService.findById(subjectForm.getId()).get();
            subject.setTitle(subjectForm.getTitle());
            subject.setTeacher(teacherService.findByLastName(subjectForm.getTeacherName()));
            subject.setEncadrant(teacherService.findByLastName(subjectForm.getEncadrantName()));
        } else {
            subject = new Subject(subjectForm.getTitle(), teacherService.findByLastName(subjectForm.getTeacherName()), teacherService.findByLastName(subjectForm.getEncadrantName()));
        }
        subjectService.saveSubject(subject);
        return "redirect:/listSubjects";

    }

    @GetMapping(value = {"/showSubjectUpdateForm/{id}"})
    public String showSubjectUpdateForm(Model model, @PathVariable(value = "id") long id){

        SubjectForm subjectForm = new SubjectForm(id, subjectService.findById(id).get().getTitle(), subjectService.findById(id).get().getTeacher().getLastName(), subjectService.findById(id).get().getEncadrant().getLastName());
        model.addAttribute("subjectForm", subjectForm);
        return "updateSubject";
    }

    @GetMapping(value = {"/deleteSubject/{id}"})
    public String deleteSubject(Model model, @PathVariable(value = "id") long id){
        subjectService.deleteSubject(id);
        return "redirect:/listSubjects";
    }
}
