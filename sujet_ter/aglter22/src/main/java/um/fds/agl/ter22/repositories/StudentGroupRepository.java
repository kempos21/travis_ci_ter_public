package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.StudentGroup;

public interface StudentGroupRepository extends CrudRepository<StudentGroup, Long> {

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    StudentGroup save(@Param("studentGroup") StudentGroup studentGroup);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    void deleteById(@Param("id") Long id);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    void delete(@Param("studentGroup") StudentGroup studentGroup);
}