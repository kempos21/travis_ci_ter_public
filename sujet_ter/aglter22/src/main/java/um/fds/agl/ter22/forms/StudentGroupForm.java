package um.fds.agl.ter22.forms;

public class StudentGroupForm {

    private long id;
    private String name;

    public StudentGroupForm(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public StudentGroupForm() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
