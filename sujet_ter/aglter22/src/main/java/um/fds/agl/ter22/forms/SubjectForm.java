package um.fds.agl.ter22.forms;

public class SubjectForm {

    private long id;
    private String title;
    private String teacherName;
    private String encadrantName;

    public SubjectForm(long id, String title, String teacherName, String encadrantName) {
        this.id = id;
        this.title = title;
        this.teacherName = teacherName;
        this.encadrantName= encadrantName;
    }

    public SubjectForm() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public String getEncadrantName(){return encadrantName;}

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public void setEncadrantName(String encadrantName) { this.encadrantName = encadrantName;}
}
